import dotenv from 'dotenv';
dotenv.config();

import * as path from 'path';
const { env } = process;

export const MAX_AXIOS_TIMEOUT = Number(env.MAX_AXIOS_TIMEOUT) || 30_000;
export const AXIOS_RETRY_ATTEMPT_TIMEOUT =
  Number(env.AXIOS_RETRY_ATTEMPT_TIMEOUT) || 500;
export const AXIOS_MAX_RETRIES = Number(env.AXIOS_MAX_RETRIES) || 15;

export const API_URL = env.API_URL || '';
export const BASE_URL = env.BASE_URL || '';
export const IMG_CDN_BASE_URL = env.IMG_CDN_BASE_URL || '';
export const SELLING = env.SELLING || '';
export const RENTING = env.RENTING || '';

export const ALL_LOGS_PATH =
  env.ALL_LOGS_PATH || path.join(__dirname, '../../var/all.log');
export const ERROR_LOGS_PATH =
  env.ERROR_LOGS_PATH || path.join(__dirname, '../../var/error.log');

export const APP_NAME = env.APP_NAME || '';
export const APP_PORT = env.APP_PORT || '';
export const IN_PRODUCTION = env.NODE_ENV === 'production';

export const AUTH0_BASE_URL = env.AUTH0_BASE_URL || '';
export const AUTH0_CLIENT_ID = env.AUTH0_CLIENT_ID || '';
export const AUTH0_CLIENT_SECRET = env.AUTH0_CLIENT_SECRET || '';
export const AUTH0_AUDIENCE = env.AUTH0_AUDIENCE || '';

export const QUEUE_DSN = env.QUEUE_DSN || '';
export const QUEUE_CHANNEL_NAME = env.QUEUE_CHANNEL_NAME || '';
export const QUEUE_SELL_CRON = env.QUEUE_SELL_CRON || '';
export const QUEUE_RENT_CRON = env.QUEUE_RENT_CRON || '';

export const QUEUE_SELL_HEALTHCHECK_URL = env.QUEUE_SELL_HEALTHCHECK_URL || '';
export const QUEUE_RENT_HEALTHCHECK_URL = env.QUEUE_RENT_HEALTHCHECK_URL || '';
