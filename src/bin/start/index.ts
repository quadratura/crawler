import crawler from '../../services/crawler';

(async () => {
  const user_input = process.argv[2] || '';
  if (!['sell', 'rent'].includes(user_input)) {
    throw new Error(
      `Argument "${user_input}" is invalid. Allowed values: "sell", "rent".`
    );
  }
  const type: 'rent' | 'sell' = user_input as 'rent' | 'sell';
  await crawler(type);
})();
