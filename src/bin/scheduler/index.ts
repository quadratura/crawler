import http from 'http';
import axios from 'axios';
import { CronJob } from 'cron';
import {
  APP_PORT,
  QUEUE_SELL_CRON,
  QUEUE_RENT_CRON,
  QUEUE_SELL_HEALTHCHECK_URL,
  QUEUE_RENT_HEALTHCHECK_URL,
} from '../../config';
import crawler from '../../services/crawler';

function startHealthCheckServer() {
  http
    .createServer(function(req, res) {
      res.write('OK');
      res.end();
    })
    .listen(APP_PORT);
}

function startScheduler() {
  new CronJob(QUEUE_SELL_CRON, async () => {
    axios.get(QUEUE_SELL_HEALTHCHECK_URL);
    await crawler('sell');
  }).start();
  new CronJob(QUEUE_RENT_CRON, async () => {
    axios.get(QUEUE_RENT_HEALTHCHECK_URL);
    await crawler('rent');
  }).start();
}

(async () => {
  startHealthCheckServer();
  startScheduler();
})();
