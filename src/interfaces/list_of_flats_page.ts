export interface IListOfFlatsPage {
  list_of_flats: ListOfFlats;
}
export interface ListOfFlats {
  CategoryId: number;
  AdGeoLocations?: null;
  FacetGroups?: (FacetGroupsEntity)[] | null;
  Pivots: Pivots;
  PageNumber: number;
  TotalCount: number;
  TotalPages: number;
  MaxResultsOnMap: number;
  HasOverflowResults: boolean;
  Ads?: (AdsEntity)[] | null;
  SEOWidgetHtml?: null;
  SEOModel?: null;
  Banners?: null;
}
export interface FacetGroupsEntity {
  Code?: string | null;
  Facets?: (FacetsEntity)[] | null;
}
export interface FacetsEntity {
  Id?: string | null;
  FacetCount?: number | null;
  DisplayControl: number;
  Code?: string | null;
  Count?: number | null;
}
export interface Pivots {
}
export interface AdsEntity {
  RelativeUrl: string;
  HasAutomaticRenewal: boolean;
  ValidToProlonged?: null;
  ShowInUnifiedAdvertiserAdList: boolean;
  IsUsedMoveToTop: boolean;
  ExpiresWithin48Hours: boolean;
  AdvertiserAdsUrl?: null;
  AbsoluteUrl?: null;
  NoImageAbsoluteUrl?: null;
  UniqueId?: null;
  Id: string;
  AdKindId?: null;
  IsPromoted: boolean;
  IsInterestingInternal: boolean;
  IsInterestingExternal: boolean;
  InterestingEntryDate?: null;
  AdKindCode?: null;
  AdKindPosition: number;
  StateId: number;
  AmountForPayment: number;
  StoppageReasonIds?: null;
  StoppageReasonDescription?: null;
  Version: number;
  Stamp: string;
  AdvertiserId: string;
  Title: string;
  Text?: null;
  TextHtml?: null;
  PrintTitle?: null;
  PrintText?: null;
  PrintAdvertiser?: null;
  ContactInfoName?: null;
  PhoneNumber1?: null;
  PhoneNumber2?: null;
  Email?: null;
  Address?: null;
  City?: null;
  ValidFrom?: null;
  ValidFromForDisplay?: null;
  ValidFromProlonged?: null;
  ValidTo?: null;
  LastPublished?: null;
  IsFirstOfKind: boolean;
  CreatedAt: string;
  LastModifiedAt?: null;
  ImportTypeId: number;
  GeoLocationRPT?: null;
  ImageCount?: null;
  ImageURLs?: null;
  ImageTexts?: null;
  CategoryIds?: null;
  CategoryId: number;
  CategoryHierarchyId?: null;
  CategoryNames?: null;
  CategoryFullName?: null;
  AdvertiserLogoUrlInternal?: null;
  AdvertiserLogoUrl?: null;
  VideoUrl?: null;
  CreatedByUserId: number;
  DeclarationId?: null;
  EnclosureFilePath?: null;
  ListHTML: string;
  GridHTML?: null;
  DoNotShowContactButton?: null;
  ContactButtonLink?: null;
  OtherFields?: null;
  IsVerificationPending: boolean;
  VerificationStateId: number;
  InfoMessage?: null;
  TotalViews: number;
  TopCategoryCSSClass?: null;
  JobApplicationCount: number;
  ShowAdvertiserAdsLink: boolean;
  ShowMyAvatar: boolean;
  IsOwnedByCurrentUser: boolean;
  ThreeDTourExists: boolean;
  UseRaiffeisenCreditCalculator: boolean;
  CreditInstalment?: null;
  CreditTotalAmount?: null;
  UseIntesaCreditCalculatorF: boolean;
  UseSberbankCreditCalculator: boolean;
}
