export interface ISingleFlatPage {
  contact: Contact;
  flat: Flat;
}
export interface Contact {
  Id: string;
  AdKindId: string;
  IsPromoted: boolean;
  StateId: number;
  AdvertiserId: string;
  Title: string;
  TextHtml: string;
  Email: string;
  ValidFrom: string;
  ValidTo: string;
  GeoLocationRPT: string;
  ImageURLs?: (string)[] | null;
  ImageTexts?: (string)[] | null;
  CategoryIds?: (number)[] | null;
  CategoryNames?: (string)[] | null;
  AdvertiserLogoUrl: string;
  VideoUrl?: null;
  EnclosureFilePath?: null;
  DoNotShowContactButton?: null;
  ContactButtonLink?: null;
  OtherFields: OtherFields;
  IsVerificationPending: boolean;
  VerificationStateId: number;
  TotalViews: number;
  IsOwnedByCurrentUser: boolean;
  UseRaiffeisenCreditCalculator: boolean;
  CreditInstalment?: null;
  CreditTotalAmount?: null;
  UseIntesaCreditCalculatorF: boolean;
  UseSberbankCreditCalculator: boolean;
  IsInterestingInternal: boolean;
  IsInterestingExternal: boolean;
  RelativeUrl: string;
  HasAutomaticRenewal: boolean;
  ValidToProlonged?: null;
  ExpiresWithin48Hours: boolean;
}
export interface OtherFields {
  grad_s: string;
  lokacija_s: string;
  mikrolokacija_s: string;
  broj_soba_s: string;
  namestenost_s: string;
  grejanje_s: string;
  kvadratura_d: number;
  oglasivac_nekretnine_s: string;
  nacin_placanja_s: string;
  sprat_od_s: string;
  tip_nekretnine_s: string;
  ulica_t: string;
  cena_d: number;
  dodatno_ss?: (string)[] | null;
  ostalo_ss?: (string)[] | null;
  sprat_s: string;
  stanje_objekta_s: string;
  grad_id_l: number;
  lokacija_id_l: number;
  mikrolokacija_id_l: number;
  broj_soba_id_l: number;
  namestenost_id_l: number;
  grejanje_id_l: number;
  oglasivac_nekretnine_id_l: number;
  nacin_placanja_id_l: number;
  sprat_od_id_l: number;
  tip_nekretnine_id_l: number;
  dodatno_id_ls?: (number)[] | null;
  ostalo_id_ls?: (number)[] | null;
  sprat_id_l: number;
  broj_soba_order_i: number;
  sprat_order_i: number;
  kvadratura_d_unit_s: string;
  cena_d_unit_s: string;
  defaultunit_kvadratura_d: number;
  defaultunit_cena_d: number;
  _version_: number;
}
export interface Flat {
  PhoneNumber1: string;
  PhoneNumber2: string;
  City?: null;
  Address: string;
  Email: string;
  ShowContactPhone: boolean;
  ShowMyAdsPage: boolean;
  ShowContactButton: boolean;
  Advertiser: Advertiser;
  Latitudes?: (null)[] | null;
  Longitudes?: (null)[] | null;
  InvestorRoute: string;
  UserAdsRoute: string;
  SchoolRoute: string;
  AvatarUrl: string;
  NumberInRegister: string;
  WebAddress: string;
}
export interface Advertiser {
  DisplayName: string;
  IsSchool: boolean;
  IsInvestor: boolean;
  ContactInfos?: (ContactInfosEntity)[] | null;
}
export interface ContactInfosEntity {
  Address: string;
  City?: null;
  Phone1: string;
  Phone2?: null;
  Email: string;
}
