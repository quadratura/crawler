import { createLogger, format, transports } from "winston";
import {
  ALL_LOGS_PATH,
  ERROR_LOGS_PATH,
  APP_NAME,
  IN_PRODUCTION
} from "../../config";

const logger = createLogger({
  level: "info",
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss"
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json()
  ),
  defaultMeta: { service: APP_NAME },
  transports: [
    //
    // - Write to all logs with level `info` and below to `quick-start-combined.log`.
    // - Write all logs error (and below) to `quick-start-error.log`.
    //
    new transports.File({ filename: ERROR_LOGS_PATH, level: "error" }),
    new transports.File({ filename: ALL_LOGS_PATH })
  ]
});

//
// If we're not in production then **ALSO** log to the `console`
// with the colorized simple format.
//
if (!IN_PRODUCTION) {
  logger.add(
    new transports.Console({
      format: format.combine(format.colorize(), format.simple())
    })
  );
}

export default logger;