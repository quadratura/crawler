import axios from 'axios';
import {
  AUTH0_BASE_URL,
  AUTH0_CLIENT_ID,
  AUTH0_CLIENT_SECRET,
  AUTH0_AUDIENCE,
} from '../../config';
// curl --request POST \
//   --url https://quadratura.eu.auth0.com/oauth/token \
//   --header 'content-type: application/json' \
//   --data ''

export default async function getAccessToken() {
  const {
    data: { access_token },
  } = await axios.post(`${AUTH0_BASE_URL}/oauth/token`, {
    client_id: AUTH0_CLIENT_ID,
    client_secret: AUTH0_CLIENT_SECRET,
    audience: AUTH0_AUDIENCE,
    grant_type: 'client_credentials',
  });

  return access_token;
}
