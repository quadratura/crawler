import axios from 'axios';
import * as parser from '../../services/html_to_json';
import { IFlat, ISingleFlatPage } from '../../interfaces';
import {
  MAX_AXIOS_TIMEOUT,
  AXIOS_RETRY_ATTEMPT_TIMEOUT,
  API_URL,
  AXIOS_MAX_RETRIES,
} from '../../config';
import logger from '../logger';
import getToken from '../auth';

export const get_total_pages = async (url: string): Promise<number> => {
  for (let i = 0; i < AXIOS_MAX_RETRIES; i++) {
    try {
      const { data: html } = await axios.get(url, {
        timeout: MAX_AXIOS_TIMEOUT,
      });
      return parser.parse_list_of_flats_page(html).list_of_flats.TotalPages;
    } catch (e) {
      await setTimeout(() => {}, AXIOS_RETRY_ATTEMPT_TIMEOUT);
      logger.error(e);
    }
  }

  return null;
};

export const save_flat = async (flat: IFlat): Promise<string> => {
  try {
    const token = await getToken();
    const { data: response } = await axios.post(
      API_URL,
      {
        operationName: 'SaveFlat',
        variables: { flat: JSON.stringify(flat) },
        query:
          'mutation SaveFlat($flat: String!) {save(flat_as_json: $flat) {id}}',
      },
      {
        timeout: MAX_AXIOS_TIMEOUT,
        headers: {
          authorization: `Bearer ${token}`,
          'content-type': 'application/json',
        },
      }
    );
    return response.data.save.id;
  } catch (e) {
    logger.error({ message: e.response.data });
    return e;
  }
};

export const fetch_flat = async (
  url: string
): Promise<ISingleFlatPage | null> => {
  for (let i = 0; i < AXIOS_MAX_RETRIES; i++) {
    try {
      const { data: single_flat_page_html } = await axios.get(url, {
        timeout: MAX_AXIOS_TIMEOUT,
      });
      return parser.parse_single_flat_page(single_flat_page_html);
    } catch (e) {
      await setTimeout(() => {}, AXIOS_RETRY_ATTEMPT_TIMEOUT);
      logger.error(e);
    }
  }

  return null;
};

export const fetch_flat_list_page_html = async (
  url: string
): Promise<string | null> => {
  for (let i = 0; i < AXIOS_MAX_RETRIES; i++) {
    try {
      const { data: all_flats_page_html } = await axios.get(url, {
        timeout: MAX_AXIOS_TIMEOUT,
      });

      return all_flats_page_html;
    } catch (e) {
      await setTimeout(() => {}, AXIOS_RETRY_ATTEMPT_TIMEOUT);
      logger.error(e);
    }
  }

  return null;
};
