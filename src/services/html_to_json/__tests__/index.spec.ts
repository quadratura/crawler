import * as parser from "../index";
import * as fs from "fs";
import * as path from "path";

test("it can parse single flat page", () => {
  const html = fs
    .readFileSync(
      path.join(__dirname, "../../../fixtures/single_flat_page.html")
    )
    .toString();

  expect(html).toBeTruthy();

  expect(parser.parse_single_flat_page(html)).toMatchSnapshot();
});

test("it can resolve total pages", () => {
  const html = fs
    .readFileSync(
      path.join(__dirname, "../../../fixtures/list_of_flats_page.html")
    )
    .toString();

  expect(parser.parse_list_of_flats_page(html)).toMatchSnapshot();
});
