import { IListOfFlatsPage, ISingleFlatPage } from "../../interfaces/index";

const extract_json_string = (
  startKey: string,
  endKey: string,
  text: string
): any | null => {
  const start = text.indexOf(startKey) + startKey.length;
  const end = text.indexOf(endKey, start);

  try {
    return JSON.parse(text.substr(start, end - start + endKey.length - 1));
  } catch (e) {
    return null;
  }
};

export const parse_list_of_flats_page = (
  html: string
): IListOfFlatsPage | null => {
  return {
    list_of_flats: extract_json_string(
      "QuidditaEnvironment.serverListData=",
      "};",
      html
    )
  };
};
export const parse_single_flat_page = (
  html: string
): ISingleFlatPage | null => {
  return {
    contact: extract_json_string(
      "QuidditaEnvironment.CurrentClassified=",
      "};",
      html
    ),
    flat: extract_json_string(
      "QuidditaEnvironment.CurrentContactData=",
      "};",
      html
    )
  };
};
