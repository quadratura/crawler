import * as parser from "../index";
import * as fs from "fs";
import * as path from "path";

test("it will parse list of flats page and extract links to individual flats", () => {
  const html = fs
    .readFileSync(
      path.join(__dirname, "../../../fixtures/list_of_flats_page.html")
    )
    .toString();

  expect(parser.get_relative_urls(html)).toEqual([
    "/nekretnine/prodaja-stanova/dvosoban-stan-na-limanu-1-pored-fakulteta/5425634811957?kid=4",
    "/nekretnine/prodaja-stanova/blok-19a/5425634784255?kid=4",
    "/nekretnine/prodaja-stanova/novo---sredjen-1-5-stan-48m2/5425634810787?kid=4",
    "/nekretnine/prodaja-stanova/novi-beograd---a-blok-dvosoban-id1356/5425634716992?kid=4",
    "/nekretnine/prodaja-stanova/kapije-zlatibora-lux-apartmani-direktna-proda/5425634562092?kid=4",
    "/nekretnine/prodaja-stanova/dedinje---beli-dvor-80m2-garaza-id1275/5425634539636?kid=4",
    "/nekretnine/prodaja-stanova/novi-beograd-savada-a-blok-39m2-lux-id1123/5425634257278?kid=4",
    "/nekretnine/prodaja-stanova/novi-beograd---blok-70-100m2-id1363/5425634716961?kid=4",
    "/nekretnine/prodaja-stanova/novi-beograd---blok-30-id1190/5425634461073?kid=4",
    "/nekretnine/prodaja-stanova/extra-nov-stan-u-vojvode-stepe-bez-provizije/5425634782898?kid=4",
    "/nekretnine/prodaja-stanova/dedinje-majora-jagodica-2-0-id1401/5425634809977?kid=4",
    "/nekretnine/prodaja-stanova/dedinje-sime-lozanica-2-5-id1402/5425634809976?kid=4",
    "/nekretnine/prodaja-stanova/vracar-direkt-prodaja-vise-stanova-od-63-127m/5425634707824?kid=4",
    "/nekretnine/prodaja-stanova/neponovljiva-ponudasalonac-jako-blizu-kalemeg/5425634801147?kid=4",
    "/nekretnine/prodaja-stanova/retko-u-ponudi-preporuka-odlicna-lokacija-2-5/5425634808997?kid=4",
    "/nekretnine/prodaja-stanova/vracar-kneza-ive-od-semberije-648-155000-e/5425634809341?kid=4",
    "/nekretnine/prodaja-stanova/centar-mali-salonac-lux-id1032/5425634735969?kid=4",
    "/nekretnine/prodaja-stanova/novi-beograd---blok-28-id1330/5425634605156?kid=4",
    "/nekretnine/prodaja-stanova/extra-extra-lux-stanovi--bez-provizije/5425634108413?kid=4",
    "/nekretnine/prodaja-stanova/extra-lux-stanovi-na-lekinom-brdu--bez-proviz/5425634629052?kid=4"
  ]);
});
