import cheerio from 'cheerio';

export const get_relative_urls = (html: string): string[] => {
  const $ = cheerio.load(html);
  const links: string[] = [];

  $('.row .product-item.real-estates').each((index, elem) => {
    const href_attribute = $(elem)
      .find('.ad-title a')
      .first()
      .attr('href');

    links.push(href_attribute.toString());
  });

  return links;
};
