import { URL } from 'url';
import {
  IFlat,
  ISingleFlatPage,
  EnumTagTypes,
  EnumPurchaseOption,
} from '../../interfaces';
import { BASE_URL, IMG_CDN_BASE_URL } from '../../config';
import logger from '../../services/logger';
import stringToEnum from './stringToEnum';

export const has_photos = ({ contact }: ISingleFlatPage): boolean => {
  try {
    return contact.ImageURLs.length > 4;
  } catch (e) {
    logger.error(e);
    return false;
  }
};

export const has_geolocation = ({ contact }: ISingleFlatPage): boolean => {
  try {
    const [lat, lng, ...rest] = contact.GeoLocationRPT.split(',');
    if (rest.length) {
      return false;
    }

    if (isNaN(parseFloat(lat))) {
      return false;
    }

    if (isNaN(parseFloat(lng))) {
      return false;
    }

    return true;
  } catch (e) {
    logger.error(e);
    return false;
  }
};

export const convert_flat_resource_to_api_flat = (
  { contact, flat }: ISingleFlatPage,
  purchase_option: 'rent' | 'sell'
): IFlat => {
  const url = new URL(`${BASE_URL}${contact.RelativeUrl}`);
  try {
    return {
      text: contact.Title,
      url: `${BASE_URL}${url.pathname}`,
      source: {
        url: url.host,
        name: 'Halo Oglasi',
      },
      geo_location: {
        type: 'Point',
        coordinates: [
          // https://docs.mongodb.com/manual/geospatial-queries/#geospatial-geojson
          Number(contact.GeoLocationRPT.split(',')[1]),
          Number(contact.GeoLocationRPT.split(',')[0]),
        ],
      },
      images: contact.ImageURLs.map(url => ({
        url: `${IMG_CDN_BASE_URL}${url}`,
      })),
      size: String(contact.OtherFields.kvadratura_d),
      price: String(contact.OtherFields.cena_d),
      tags: [
        ...[
          {
            text:
              purchase_option === 'sell'
                ? EnumPurchaseOption.SELL
                : EnumPurchaseOption.RENT,
            kind: EnumTagTypes.PURCHASE_OPTION,
          },
          {
            text: flat.Address,
            kind: EnumTagTypes.LOCATION_ADDRESS,
          },
          {
            text: contact.OtherFields.broj_soba_s,
            kind: EnumTagTypes.NUMBER_OF_ROOMS,
          },
          {
            text: stringToEnum(contact.OtherFields.namestenost_s),
            kind: EnumTagTypes.FURNISHED_STATUS,
          },
          {
            text: stringToEnum(contact.OtherFields.grejanje_s),
            kind: EnumTagTypes.HEATING,
          },
          {
            text: stringToEnum(contact.OtherFields.oglasivac_nekretnine_s),
            kind: EnumTagTypes.LANDLOARD,
          },
          {
            text: stringToEnum(contact.OtherFields.tip_nekretnine_s),
            kind: EnumTagTypes.PROPERTY_TYPE,
          },
          {
            text: contact.OtherFields.sprat_s,
            kind: EnumTagTypes.FLOOR,
          },
          {
            text: stringToEnum(contact.OtherFields.stanje_objekta_s),
            kind: EnumTagTypes.PROPERTY_CONDITION,
          },
        ].filter(({ text }) => Boolean(text)),
        ...[
          ...(contact.OtherFields.dodatno_ss || []),
          ...(contact.OtherFields.ostalo_ss || []),
        ]
          .filter(Boolean)
          .map(value => ({ text: value, kind: EnumTagTypes.GENERIC })),
      ],
    };
  } catch (e) {
    logger.error(e);
    return null;
  }
};
