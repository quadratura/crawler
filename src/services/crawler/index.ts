import ProgressBar from 'progress';
import * as link_extractor from '../../services/extract_links_from_html';
import {
  has_photos,
  has_geolocation,
  convert_flat_resource_to_api_flat,
} from './helpers';
import { BASE_URL, SELLING, RENTING } from '../../config';
import {
  get_total_pages,
  save_flat,
  fetch_flat,
  fetch_flat_list_page_html,
} from '../../services/api';
import logger from '../../services/logger';

const process_flat = async (
  relative_url: string,
  purchase_option: 'rent' | 'sell'
): Promise<{ id: string } | null> => {
  const flat_resource = await fetch_flat(`${BASE_URL}${relative_url}`);
  if (!Boolean(flat_resource)) {
    return null;
  }

  if (!Boolean(await has_photos(flat_resource))) {
    return null;
  }

  if (!Boolean(await has_geolocation(flat_resource))) {
    return null;
  }

  const api_flat = await convert_flat_resource_to_api_flat(
    flat_resource,
    purchase_option
  );
  if (!Boolean(api_flat)) {
    return null;
  }

  const flat_id = await save_flat(api_flat);
  if (!Boolean(flat_id)) {
    return null;
  }

  return { id: flat_id };
};

export default async function crawler(type: 'rent' | 'sell') {
  const base_url = BASE_URL + (type === 'sell' ? SELLING : RENTING);

  const total_pages = await get_total_pages(base_url);
  const progress = new ProgressBar('[⏱ :elapsed] [:bar] :percent [🏁 :etas]', {
    total: total_pages,
    complete: '=',
    incomplete: ' ',
    width: 80,
  });

  for (let page = 1; page <= total_pages; page++) {
    try {
      const all_flats_page_html = await fetch_flat_list_page_html(
        `${base_url}?page=${page}`
      );

      const relative_urls = link_extractor.get_relative_urls(
        all_flats_page_html
      );

      for (let relative_url of relative_urls) {
        await process_flat(relative_url, type);
      }
    } catch (e) {
      logger.error(e);
    }

    progress.tick();
  }
}
